import { StatusBar } from "expo-status-bar";
import React, { useEffect, useCallback } from "react";
import { StyleSheet, Text, View } from "react-native";
import MainScreen from "./screens/main-screen/MainScreen";
import { NavigationContainer } from "@react-navigation/native";
import "react-native-gesture-handler";
import MainNavi from "./navigations/MainNavi";
import { Provider as PaperProvider } from "react-native-paper";

import * as SplashScreen from "expo-splash-screen";
import { useFonts } from "expo-font";
import { Provider } from "react-redux";
import { store } from "./store/store";

SplashScreen.preventAutoHideAsync();

export default function App() {
  const [fontsLoaded] = useFonts({
    "insta-regular": require("./assets/fonts/InstagramSans-Regular.ttf"),
    "insta-bold": require("./assets/fonts/InstagramSans-Bold.ttf"),
    "insta-light": require("./assets/fonts/InstagramSans-Light.ttf"),
    "insta-cond-reg": require("./assets/fonts/InstagramSansCondensed-Regular.ttf"),
    "insta-cond-bold": require("./assets/fonts/InstagramSansCondensed-Bold.ttf"),
  });

  const onLayoutRootView = useCallback(async () => {
    if (fontsLoaded) {
      await SplashScreen.hideAsync();
    }
  }, [fontsLoaded]);
  if (!fontsLoaded) {
    return null;
  }
  return (
    <Provider store={store}>
      <PaperProvider>
        <View onLayout={onLayoutRootView} style={styles.container}>
          <MainNavi></MainNavi>
        </View>
      </PaperProvider>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    // alignItems: "center",
    // justifyContent: "center",
  },
});
