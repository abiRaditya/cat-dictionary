import React, { useEffect, useCallback } from "react";
import { View, Text, StyleSheet, StatusBar, Appearance } from "react-native";
// nav
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

//screen
import MainScreen from "../screens/main-screen/MainScreen";
import SearchBreed from "../screens/search-breed/SearchBreed";
import BreedDetail from "../screens/breed-detail/BreedDetail";
import { FontAwesome, Entypo } from "@expo/vector-icons";

// SplashScreen.preventAutoHideAsync();

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();

const SearchStack = () => {
  return (
    <Stack.Navigator
      screenOptions={() => ({
        headerShown: false,
      })}
    >
      <Stack.Screen name="search-breed" component={SearchBreed}></Stack.Screen>
      <Stack.Screen name="breed-detail" component={BreedDetail}></Stack.Screen>
    </Stack.Navigator>
  );
};
const MainNavi = () => {
  // const theme = Appearance.getColorScheme();
  // const themeObj = {
  //   light: `light-content`,
  //   dark: `dark-content`,
  // };

  return (
    <NavigationContainer>
      <StatusBar
        animated={true}
        barStyle={`dark-content`}
        backgroundColor={`white`}
      />
      <BottomTab.Navigator
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarHideOnKeyboard: true,
          tabBarIcon: ({ color, size }) => {
            let iconName;
            if (route.name == "Breed") {
              // iconName ='' ;
              return <Entypo name="home" size={size} color={color} />;
            } else if (route.name == "Random") {
              return <FontAwesome name="random" size={size} color={color} />;
            }
          },
          tabBarActiveTintColor: "black",
          tabBarInactiveTintColor: "gray",
          tabBarShowLabel: false,
        })}
      >
        <BottomTab.Screen
          name="Breed"
          component={MainScreen}
        ></BottomTab.Screen>
        <BottomTab.Screen
          name="Random"
          component={SearchStack}
        ></BottomTab.Screen>
      </BottomTab.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({});

export default MainNavi;
