import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getBreedsThunk } from "../repository/asyncThunk";
const useDispatchBreeds = ({ page, limit }, resetPage) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getBreedsThunk({ page, limit }));
  }, [page]);
};

export default useDispatchBreeds;
