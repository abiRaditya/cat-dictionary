import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getFavorites } from "../repository/asyncThunk";

const useGetFavorites = () => {
  const favorites = useSelector((state) => state.mainscreen.favorites);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getFavorites());
  }, []);
  return [favorites];
};

export default useGetFavorites;
