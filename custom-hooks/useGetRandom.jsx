import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getSearchRandom } from "../repository/asyncThunk";

const useGetRandom = ({ page, limit }) => {
  const randoms = useSelector((state) => state.randomscreen.randoms);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getSearchRandom({ page, limit }));
  }, [page]);
  return [randoms];
};

export default useGetRandom;
