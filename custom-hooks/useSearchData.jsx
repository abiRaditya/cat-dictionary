import { useState, useEffect } from "react";
import { useSelector } from "react-redux";

const useSearchData = ({ searchQuery }) => {
  const breeds = useSelector((state) => state.mainscreen.catBreeds);
  const [breedSearchResult, setBreedSearchResult] = useState(breeds);
  function searchQ(q) {
    if (!q) {
      setBreedSearchResult(breeds);
      return;
    }
    const searchResult = breeds.filter((breed) => {
      return breed.name.toLowerCase().includes(q.toLowerCase());
    });
    setBreedSearchResult(searchResult);
  }
  useEffect(() => {
    if (searchQuery) {
      searchQ(searchQuery);
    } else if (!searchQuery) {
      setBreedSearchResult(breeds);
    }
  }, [searchQuery, breeds]);

  return [breedSearchResult];
};

export default useSearchData;
