import { configureStore } from "@reduxjs/toolkit";
import mainScreenReducer from "../screens/main-screen/mainScreenSlice";
import randomReducer from "../screens/search-breed/randomSlice";
export const store = configureStore({
  reducer: {
    mainscreen: mainScreenReducer,
    randomscreen: randomReducer,
  },
});
