export default {
  app: {
    flex: 1,
    // padding: 15,
    // paddingLeft: 15,
    // paddingRight: 15,
    paddingTop: 5,
    backgroundColor: "white",
  },
  paragraph: {
    fontSize: 14,
    lineHeight: 19,
  },
};
