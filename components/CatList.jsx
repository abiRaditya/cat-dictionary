import React from "react";
import { StyleSheet, FlatList } from "react-native";
import EmptyList from "./EmptyList";
import FooterList from "./FooterList";
import { useDispatch } from "react-redux";

import {
  postFavorites,
  deleteFavorite,
  getFavorites,
} from "../repository/asyncThunk";
import useGetFavorites from "../custom-hooks/useGetFavorites";
import CatCard from "./CatCard";

const CatList = ({ catListDatas, isLoading, isListEnd, fetchMoreData }) => {
  const dispatch = useDispatch();
  const [favorites] = useGetFavorites();
  function isImageLiked(image_id) {
    if (!image_id) {
      return false;
    }
    for (let i = 0; i < favorites.length; i++) {
      const fav = favorites[i];
      if (fav.image_id == image_id) {
        return fav.id;
      }
    }
    return false;
  }

  function likeMethod(image_id) {
    // console.log(image_id, "image_id");
    const isLiked = isImageLiked(image_id);
    if (!isLiked) {
      dispatch(postFavorites(image_id));
    } else {
      dispatch(deleteFavorite(isLiked));
    }
    setTimeout(() => {
      dispatch(getFavorites());
    }, 100);
  }

  return (
    <FlatList
      renderItem={({ item }) => {
        return (
          <CatCard
            breedName={item.name}
            imageUri={item.image_url}
            origin={item.origin}
            description={item.description}
            isLiked={isImageLiked(item.image_id)}
            likeMethod={() => {
              likeMethod(item.image_id);
            }}
          ></CatCard>
        );
      }}
      data={catListDatas}
      // data={[]}
      keyExtractor={(item, index) => item.id + index}
      ListEmptyComponent={<EmptyList />}
      ListFooterComponent={() => (
        <FooterList isLoading={isLoading} isListEnd={isListEnd} />
      )}
      onEndReached={fetchMoreData}
      onEndReachedThreshold={0.2}
    ></FlatList>
  );
};

const styles = StyleSheet.create({});

export default CatList;
