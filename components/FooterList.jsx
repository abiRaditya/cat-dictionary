import React from "react";
import { View, StyleSheet, ActivityIndicator, Text } from "react-native";

const FooterList = ({ isLoading, isListEnd }) => {
  return (
    <View>
      {isLoading && <ActivityIndicator size="large" color="#000" />}
      {isListEnd && <Text>This is the end of the list</Text>}
    </View>
  );
};

const styles = StyleSheet.create({});

export default FooterList;
