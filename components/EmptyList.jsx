import React from "react";
import { View, StyleSheet, Text } from "react-native";

const EmptyList = () => {
  return (
    <View style={styles.container}>
      <Text>Try changing the search query / retry load</Text>
      {/* <Text>Retry</Text> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "red",
    flexDirection: "column",
    height: "100%",
  },
});

export default EmptyList;
