import React from "react";
import { View, StyleSheet, TouchableOpacity, Animated } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
const HeartButton = ({ isLiked, style, likeMethod }) => {
  const animatedButtonScale = new Animated.Value(1);
  const onPressIn = () => {
    Animated.spring(animatedButtonScale, {
      toValue: 1.5,
      useNativeDriver: true,
    }).start();
  };
  const onPressOut = () => {
    Animated.spring(animatedButtonScale, {
      toValue: 1,
      useNativeDriver: true,
    }).start();
  };
  const heartOnPress = likeMethod
    ? likeMethod
    : () => {
        console.log("heart pressed");
      };
  return (
    <TouchableOpacity
      onPressIn={onPressIn}
      onPressOut={onPressOut}
      onPress={heartOnPress}
      style={style}
    >
      <Animated.View>
        <FontAwesome
          name={isLiked ? "heart" : "heart-o"}
          color={isLiked ? "red" : "black"}
          size={24}
        />
      </Animated.View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({});

export default HeartButton;
