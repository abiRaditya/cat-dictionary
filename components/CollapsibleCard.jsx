import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Animated,
} from "react-native";
// import { ChevronsDown } from "react-feather";
import { Entypo, Feather } from "@expo/vector-icons";
import { useSpring, animated, config } from "@react-spring/native";

import bezier from "bezier-easing";

import HeartButton from "./HeartButton";

const propTypes = {
  children: PropTypes.node,
  contentHeight: PropTypes.number,
  defaultCollapsed: PropTypes.bool,
  style: PropTypes.any,
  title: PropTypes.string,
  useBezier: PropTypes.bool,
};

const defaultProps = {
  contentHeight: 300,
};

function CollapsibleCard({
  children,
  contentHeight,
  defaultCollapsed,
  style,
  title,
  useBezier,
  isLiked,
  imageUri,
  likeMethod,
  ...props
}) {
  const [isCollapsed, setCollapsed] = useState(
    defaultCollapsed ? defaultCollapsed : true
  );

  const animationConfig = {
    height: isCollapsed ? 0 : contentHeight,
    progress: isCollapsed ? 0 : 100,
    rotation: isCollapsed ? `0deg` : `-180deg`,
  };

  if (useBezier) {
    animationConfig.config = {
      duration: 600,
      easing: (t) => bezier(0.25, 0, 0, 1)(t),
    };
  }

  const animation = useSpring(animationConfig);
  const AnimatedView = animated(View);

  return (
    <View {...props} style={[styles.card, style]}>
      {/* Card Top */}
      <View style={styles.cardTop}>
        <View>
          <Image style={styles.image} source={{ uri: imageUri }} />
          <HeartButton
            style={styles.heartButton}
            isLiked={isLiked}
            likeMethod={likeMethod}
          />
          <Text style={styles.cardTitle}>
            {isLiked ? "You liked this" : ""}
          </Text>
          <View style={styles.titleContainer}>
            <Text style={styles.cardTitle}>Cat breed</Text>
            <Text style={{ fontFamily: "insta-regular" }}>{title}</Text>
          </View>
        </View>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => setCollapsed((currentBool) => !currentBool)}
        >
          <AnimatedView style={{ transform: [{ rotate: animation.rotation }] }}>
            {/* <ChevronDown /> */}
            {/* <ChevronsDown></ChevronsDown> */}
            <Entypo name="chevron-down" size={24} color="black" />
            {/* <Text>A</Text> */}
          </AnimatedView>
        </TouchableOpacity>
      </View>

      {/* Card Content */}
      <AnimatedView
        style={[
          styles.cardContent,
          {
            height: animation.height,
            borderTopWidth: animation.progress.to({
              range: [0, 25, 50, 75, 100],
              output: [0, 0, 0, 0, 1],
            }),
            opacity: animation.progress.to({
              range: [0, 85, 95, 100],
              output: [0, 0, 0.5, 1],
            }),
          },
        ]}
      >
        {/* Inner */}
        <AnimatedView
          style={{
            transform: [
              {
                translateY: animation.progress.to({
                  range: [0, 85, 95, 100],
                  output: [7.5, 5, 2.5, 0],
                }),
              },
            ],
          }}
        >
          {children}
        </AnimatedView>
      </AnimatedView>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "white",
    borderRadius: 6,
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    marginTop: 20,
  },
  cardTop: {
    flexDirection: "column",
    alignItems: "center",
    // justifyContent: "space-between",
    padding: 8,
    borderTopWidth: 1,
    borderTopColor: "#f7f7f7",
    marginTop: 5,
    paddingTop: 20,
  },
  cardContent: {
    borderTopWidth: 1,
    borderColor: "#fff",
  },
  image: {
    width: 380,
    height: 380,
    borderRadius: 3,
    backgroundColor: "black",
  },
  titleContainer: {
    flexDirection: "row",
    marginTop: 5,
  },
  cardTitle: {
    fontFamily: "insta-bold",
    marginRight: 10,
  },
  heartButton: {
    marginTop: 10,
  },
});

CollapsibleCard.propTypes = propTypes;
CollapsibleCard.defaultProps = defaultProps;

export default CollapsibleCard;
