import React from "react";
import { View, StyleSheet, Text } from "react-native";
import CollapsibleCard from "./CollapsibleCard";
import appStyle from "../constants/appStyle";

const CatCard = ({
  isLiked,
  breedName,
  imageUri,
  origin,
  description,
  likeMethod,
}) => {
  return (
    <CollapsibleCard
      isLiked={isLiked}
      title={breedName}
      imageUri={imageUri}
      useBezier={true}
      likeMethod={likeMethod}
    >
      <View style={styles.container}>
        <View style={styles.paraContainer}>
          <Text style={styles.paragraphTitle}>Origin:</Text>
          <Text style={styles.paragraphDesc}>{origin}</Text>
        </View>
        <Text style={styles.paragraph}>{description}</Text>
      </View>
    </CollapsibleCard>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingRight: 20,
    paddingLeft: 20,
  },
  paragraph: {
    fontSize: 20,
    fontFamily: "insta-cond-reg",
  },
  paragraphTitle: {
    fontSize: 20,
    fontFamily: "insta-cond-bold",
  },
  paragraphDesc: {
    fontSize: 20,
    fontFamily: "insta-cond-reg",
    marginLeft: 10,
  },
  paraContainer: {
    flexDirection: "row",
    marginBottom: 10,
  },
});

export default CatCard;
