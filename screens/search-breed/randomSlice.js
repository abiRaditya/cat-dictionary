import { createSlice } from "@reduxjs/toolkit";
import {
  getSearchRandom,
  postFavorites,
  deleteFavorite,
} from "../../repository/asyncThunk";

const initialState = {
  randoms: [],
  isLoading: false,
  maxPage: null,
  isShowSnackbar: false,
};

export const randomSlice = createSlice({
  name: "randomslice",
  initialState,
  reducers: {
    randomSnackBar: (state, { payload }) => {
      state.isShowSnackbar = payload;
    },
  },
  extraReducers: {
    [getSearchRandom.pending]: (state) => {
      state.isLoading = true;
    },
    [getSearchRandom.fulfilled]: (state, { payload }) => {
      state.isLoading = false;
      state.randoms = state.randoms.concat(payload.data);
      state.maxPage = Math.ceil(+payload.totalItemCount / 10);
    },
    [getSearchRandom.rejected]: (state) => {
      state.isLoading = false;
    },
    [postFavorites.fulfilled]: (state) => {
      state.isShowSnackbar = true;
    },
    [deleteFavorite.fulfilled]: (state) => {
      state.isShowSnackbar = true;
    },
  },
});
export const { randomSnackBar } = randomSlice.actions;
export default randomSlice.reducer;
