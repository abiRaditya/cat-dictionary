import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import appStyle from "../../constants/appStyle";

import useGetRandom from "../../custom-hooks/useGetRandom";

import { useSelector, useDispatch } from "react-redux";
import { randomSnackBar } from "./randomSlice";
import { Snackbar } from "react-native-paper";
import CatList from "../../components/CatList";

const SearchBreed = ({ navigation }) => {
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(0);
  const [isListEnd, setIsListEnd] = useState(false);

  const maxPage = useSelector((state) => state.randomscreen.maxPage);
  const isLoading = useSelector((state) => state.randomscreen.isLoading);
  const isSnackVisible = useSelector(
    (state) => state.randomscreen.isShowSnackbar
  );

  function onDismissSnackBar() {
    dispatch(randomSnackBar(false));
  }

  function fetchMoreData() {
    if (typeof maxPage == "number") {
      if (currentPage >= maxPage) {
        setIsListEnd(true);
        return;
      }
    }

    setCurrentPage((currentpage) => currentpage + 1);
  }

  const [randoms] = useGetRandom({ page: currentPage, limit: 10 });
  return (
    <View style={styles.app}>
      <CatList
        catListDatas={randoms}
        isLoading={isLoading}
        isListEnd={isListEnd}
        fetchMoreData={fetchMoreData}
      ></CatList>
      <Snackbar visible={isSnackVisible} onDismiss={onDismissSnackBar}>
        Success
      </Snackbar>
    </View>
  );
};

const styles = StyleSheet.create({
  ...appStyle,
});

export default SearchBreed;
