import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import appStyle from "../../constants/appStyle";
import { Searchbar, Snackbar } from "react-native-paper";

import useDispatchBreeds from "../../custom-hooks/useGetBreeds";
import useSearchData from "../../custom-hooks/useSearchData";

import { useSelector, useDispatch } from "react-redux";
import { snackBar } from "./mainScreenSlice";

import CatList from "../../components/CatList";

const MainScreen = () => {
  const dispatch = useDispatch();
  const [text, onChangeText] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const [isListEnd, setIsListEnd] = useState(false);

  const breedsLoading = useSelector((state) => state.mainscreen.isBreedLoading);
  const maxPage = useSelector((state) => state.mainscreen.breedsMaxPage);
  const isSnackVisible = useSelector(
    (state) => state.mainscreen.isShowSnackbar
  );
  function onDismissSnackBar() {
    dispatch(snackBar(false));
  }
  // const currentPage = useRef(0);
  function fetchMoreData() {
    if (text) {
      return;
    }
    if (typeof maxPage == "number") {
      if (currentPage >= maxPage) {
        setIsListEnd(true);
        return;
      }
    }

    setCurrentPage((currentpage) => currentpage + 1);
  }

  useDispatchBreeds({ page: currentPage, limit: 10 }, () => {
    setCurrentPage(0);
  });
  const [breedSearchResult] = useSearchData({ searchQuery: text });

  return (
    <View style={styles.app}>
      <View style={styles.searchContainer}>
        {/* <Text>
          {currentPage} - {maxPage} - {favorites.length}
        </Text> */}
        <Searchbar
          iconColor="black"
          placeholder="Search"
          onChangeText={(q) => {
            onChangeText(q);
          }}
          value={text}
          style={styles.searchbar}
        />
      </View>
      <CatList
        catListDatas={breedSearchResult}
        isLoading={breedsLoading}
        isListEnd={isListEnd}
        fetchMoreData={fetchMoreData}
      ></CatList>

      <Snackbar visible={isSnackVisible} onDismiss={onDismissSnackBar}>
        Success
      </Snackbar>
    </View>
  );
};

const styles = StyleSheet.create({
  ...appStyle,
  // input: {
  //   height: 30,
  //   margin: 12,
  //   borderWidth: 1,
  //   padding: 6,
  //   fontFamily: "insta-regular",
  // },
  searchbar: {
    backgroundColor: "#efefef",
    marginTop: 5,
    marginBottom: 10,
    borderRadius: 7,
    height: 30,
  },
  searchContainer: {
    backgroundColor: "white",
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export default MainScreen;
