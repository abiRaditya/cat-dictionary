import { createSlice } from "@reduxjs/toolkit";
import {
  getBreedsThunk,
  getFavorites,
  postFavorites,
  deleteFavorite,
} from "../../repository/asyncThunk";

const initialState = {
  catBreeds: [],
  isBreedLoading: false,
  breedsMaxPage: null,
  favorites: [],
  isShowSnackbar: false,
};

export const mainScreenSlice = createSlice({
  name: "mainScreen",
  initialState,
  reducers: {
    resetCatBreeds: (state) => {
      state.catBreeds = [];
    },
    snackBar: (state, { payload }) => {
      state.isShowSnackbar = payload;
    },
  },
  extraReducers: {
    [getBreedsThunk.pending]: (state) => {
      state.isBreedLoading = true;
    },
    [getBreedsThunk.fulfilled]: (state, { payload }) => {
      state.isBreedLoading = false;
      state.catBreeds = state.catBreeds.concat(payload.data);
      state.breedsMaxPage = Math.ceil(+payload.totalItemCount / 10);
    },
    [getBreedsThunk.rejected]: (state) => {
      state.isBreedLoading = false;
    },
    [getFavorites.fulfilled]: (state, { payload = [] }) => {
      state.favorites = payload;
    },
    [postFavorites.fulfilled]: (state, { payload }) => {
      state.isShowSnackbar = true;
    },
    [deleteFavorite.fulfilled]: (state) => {
      state.isShowSnackbar = true;
    },
  },
});
export const { resetCatBreeds, snackBar } = mainScreenSlice.actions;
export default mainScreenSlice.reducer;
