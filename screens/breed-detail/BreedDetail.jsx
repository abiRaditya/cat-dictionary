import React from "react";
import { View, StyleSheet, Text, Button } from "react-native";
import appStyle from "../../constants/appStyle";

const BreedDetail = ({ navigation }) => {
  return (
    <View style={styles.app}>
      <Button title="Go Back" onPress={() => navigation.goBack()} />
      <Text>Breed Detail Clicked Through Search</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  ...appStyle,
});

export default BreedDetail;
