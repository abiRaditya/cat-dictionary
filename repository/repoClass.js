import catApi from ".";
import { API_KEY } from "@env";
const sub_id = "abirad098";
class CatApiRepo {
  async getBreeds({ page, limit }) {
    try {
      const response = await catApi({
        url: "/breeds",
        method: "get",
        params: {
          page,
          limit,
        },
      });
      return {
        data: response.data.map((item) => {
          return {
            id: item.id,
            name: item.name,
            image_url: item.image.url,
            image_id: item.image.id,
            origin: item.origin,
            description: item.description,
          };
        }),
        totalItemCount: response.headers["pagination-count"],
      };
    } catch (error) {
      console.log(error, "getBreeds");
    }
  }
  async getSearchRandom({ page, limit }) {
    try {
      const response = await catApi({
        url: "/images/search",
        method: "get",
        params: {
          api_key: API_KEY,
          page,
          limit,
          has_breeds: 1,
        },
      });
      return {
        data: response.data.map((item) => {
          return {
            id: item.id,
            name: item.breeds[0].name,
            image_url: item.url,
            image_id: item.id,
            origin: item.breeds[0].origin,
            description: item.breeds[0].description,
          };
        }),
        totalItemCount: response.headers["pagination-count"],
      };
    } catch (error) {
      console.log(error, "getBreeds");
    }
  }
  async getFavorites() {
    try {
      const response = await catApi({
        url: "/favourites",
        method: "get",
        params: {
          sub_id: sub_id,
        },
      });
      return response.data;
    } catch (error) {
      console.log(error, "getFavorites");
    }
  }
  async postFavorites(imageId) {
    try {
      const payload = {
        image_id: imageId,
        sub_id: sub_id,
      };
      const response = await catApi({
        url: "/favourites",
        method: "post",
        data: payload,
      });
      return response.data;
    } catch (error) {
      console.log(error, "postFavorites");
    }
  }

  async deleteFavorite(favId) {
    try {
      console.log(favId, "favId");
      const response = await catApi({
        url: `/favourites/${favId}`,
        method: "delete",
      });
      return response.data;
    } catch (error) {
      console.log(error, "deleteFavorite");
    }
  }
}

export default new CatApiRepo();
