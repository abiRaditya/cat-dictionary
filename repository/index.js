import axios from "axios";
import { API_KEY } from "@env";
const catApi = axios.create({
  baseURL: "https://api.thecatapi.com/v1",
  headers: {
    "x-api-key": API_KEY,
  },
});

export default catApi;
