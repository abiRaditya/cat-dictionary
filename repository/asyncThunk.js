import { createAsyncThunk } from "@reduxjs/toolkit";
import repoClass from "./repoClass";

export const getBreedsThunk = createAsyncThunk(
  `catBreed/pagination`,
  async ({ page, limit }, { rejectWithValue }) => {
    try {
      const response = await repoClass.getBreeds({ page, limit });

      //   console.log(response, "getBreedsThunk resposne data ");
      return response;
    } catch (error) {
      console.log(error, "error createAsyncThunk getBreedsThunk");
      rejectWithValue(error);
    }
  }
);
export const getSearchRandom = createAsyncThunk(
  `getSearchRandom/get`,
  async ({ page, limit }, { rejectWithValue }) => {
    try {
      const response = await repoClass.getSearchRandom({ page, limit });

      // console.log(response, "getSearchRandom resposne data ");
      return response;
    } catch (error) {
      console.log(error, "error createAsyncThunk getSearchRandom");
      rejectWithValue(error);
    }
  }
);

export const getFavorites = createAsyncThunk(
  `getFavorites/get`,
  async (nul, { rejectWithValue }) => {
    try {
      console.log("getFavorites");
      const response = await repoClass.getFavorites();

      // console.log(response, "getFavorites resposne data ");
      return response;
    } catch (error) {
      console.log(error, "error createAsyncThunk getFavorites");
      rejectWithValue(error);
    }
  }
);

export const postFavorites = createAsyncThunk(
  `postFavorites/post`,
  async (payload, { rejectWithValue }) => {
    try {
      const response = await repoClass.postFavorites(payload);

      // console.log(response, "postFavorites resposne data ");
      return response;
    } catch (error) {
      console.log(error, "error createAsyncThunk postFavorites");
      rejectWithValue(error);
    }
  }
);
export const deleteFavorite = createAsyncThunk(
  `deleteFavorite/delete`,
  async (payload, { rejectWithValue }) => {
    try {
      const response = await repoClass.deleteFavorite(payload);

      // console.log(response, "deleteFavorite resposne data ");
      return response;
    } catch (error) {
      console.log(error, "error createAsyncThunk deleteFavorite");
      rejectWithValue(error);
    }
  }
);
